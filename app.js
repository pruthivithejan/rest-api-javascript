//Package Imports
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require('mongoose');
const ejs = require('ejs');

const app = express();

app.set('view engine', 'ejs')

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(express.static("public"));

// MongoDB server setup with Mongoose
mongoose.set('strictQuery', false);
mongoose.connect("mongodb://127.0.0.1:27017/wikiDB", {
  useNewUrlParser: true
});

const articleSchema = {
  title: String,
  content: String
};

const Article = mongoose.model("Article", articleSchema);

//========================== Requests for all articles ==========================
app.route("/articles")
 //GET
.get((req, res) => {
  Article.find(function(err, foundArticles){
    if(err){
      res.send(err);
    }else{
      res.send(foundArticles);
    }  
  });
})
//POST
.post((req, res) =>{
  const newArticle = new Article({
    title:req.body.title, 
    content: req.body.content
  });

  newArticle.save(function (err) {
    if(err){
      res.send(err);
    }else{
      res.send("Article added Successfully!");
    }  
  });
})
//DELETE
.delete((req, res) => {
  Article.deleteMany(function(err){
    if(err){
      res.send(err);
    }else{
      res.send("All article deleted Successfully!");
    } 
  });
});

//========================== Requests for specific article ==========================
app.route("/articles/:articleTitle")
//GET
.get((req, res) =>{
  Article.findOne({title:req.params.articleTitle}, function(err, foundArticle){
    if(err){
      res.send(err);
    }else if(foundArticle){
      res.send(foundArticle);
    }else{
      res.send("No Article Found!")
    }  
  });
})
//PUT
.put((req,res)=>{
  Article.updateOne(
    {title: req.params.articleTitle},
    {title: req.body.title, content:req.body.content}, 
     function(err){
      if(err){
        res.send(err);
      }else{
        res.send("Article updated Successfully!");
      } 
    });
})
//PATCH
.patch((req, res)=>{
  Article.updateOne(
    {title: req.params.articleTitle},
     function(err){
      if(err){
        res.send(err);
      }else{
        res.send("Article updated Successfully!");
      } 
    });
})
//DELETE
.delete((req,res)=>{
  Article.deleteOne(
    {title: req.params.articleTitle},
    {$set: req.body}, 
     function(err){
      if(err){
        res.send(err);
      }else{
        res.send("Article deleted Successfully!");
      } 
    });
});

app.listen(3000, function() {
  console.log("Server started on http://localhost:3000/articles.");
});

